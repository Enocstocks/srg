#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>
#include <string.h>
#include <stdlib.h>
#include "../src/hash.h"

struct hash_table table;

static int setup(void ** state)
{
  alloc_hash_table(table, 36);
  hash_insert(table, 'A', 99);
  hash_insert(table, 'B', 1);
  hash_insert(table, '0', 9);
  hash_insert(table, '9', 15);
  return 0;
}

static void hash(void ** state)
{
  assert_int_equal(21, hash_function('L'));
}

static void insert(void ** state)
{
  assert_int_equal(0, hash_insert(table, 'L', 77));
  assert_int_equal('L', table.table[21]->key);
  assert_int_equal(77, table.table[21]->value);
}

static void search(void ** state)
{
  struct hash_entry * found_entry = hash_search(table, 'A');
  assert_int_equal(99, found_entry->value);
  assert_int_equal('A', found_entry->key);

  found_entry = hash_search(table, '0');
  assert_int_equal(9, found_entry->value);
  assert_int_equal('0', found_entry->key);
}

static int teardown(void ** state)
{
  hash_delete(table, 'A');
  hash_delete(table, 'B');
  hash_delete(table, '0');
  hash_delete(table, '9');
  hash_delete(table, 'L');
  free(table.table);
  return 0;
}

int main()
{
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(insert),
    cmocka_unit_test(hash),
    cmocka_unit_test(search),
    /* cmocka_unit_test(overwrite_entry), */
    /* cmocka_unit_test(search_in_chain), */
    /* cmocka_unit_test(remove_entry),  */
  };

  return cmocka_run_group_tests(tests, setup, teardown);
}
