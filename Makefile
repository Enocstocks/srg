.phony: run tests val-test val

CFLAGS = -ggdb -O0 -Wall -Wextra -std=c11
ctestflags = -O0 -ggdb -std=c11
cmockaflags = `pkg-config --libs --cflags cmocka`
objfiles = src/args.c src/data.c src/hash.c src/main.c src/structures.c
headers = src/args.h src/data.h src/hash.h src/structures.h

srg: $(objfiles) $(headers)
	gcc $(CFLAGS) -o srg $(objfiles)

run:
	./srg --dataset dataset.txt

tests: tests/hash
	./tests/hash

tests/hash: tests/hash-table.c src/hash.c
	$(CC) $(ctestflags) -o tests/hash $^ $(cmockaflags)

val-test:
	valgrind --leak-check=yes ./tests/hash

val:
	valgrind --leak-check=yes --leak-check=full --show-leak-kinds=all -s ./srg --dataset dataset.txt

perf:
	perf stat -d -r 100 ./srg --dataset dataset.txt
