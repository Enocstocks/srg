/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

int hash_function(char character)
{
  int result;

  // heuristic to know if is a character represiting a number
  if (character & 32)
    result = character - 48;
  else
    result = character - 65 + 10;

  return result;
}

/*
  Insert the hashed value of ~key~ in ~hash_table~ with the value in ~value~.

  On success:
  Return 0.

  On failure:
  Return 1. This means the new entry is already present in ~hash_table~.
*/

int hash_insert(struct hash_table table, char key, int value)
{
  int hashed_key = hash_function(key);
  struct hash_entry ** destination = &(table.table[hashed_key]);

  if ((*destination) != NULL)
    return 1;

  *destination = malloc(sizeof(struct hash_entry));
  (*destination)->value = value;
  (*destination)->key = key;
  return 0;
}

/*
  Search ~key~ in ~hash_table~.

  On success:
  Return the found entry.

  On failure:
  Return NULL.
*/
struct hash_entry * hash_search(struct hash_table table, char key)
{
  int hashed_key = hash_function(key);
  struct hash_entry * entry = table.table[hashed_key];

  if (entry == NULL)
    return NULL;

  if (entry->key == key) {
    #ifdef DEBUG_SIMPLE_HASH
    printf("Key %c found at hashed key %d\n", key, hashed_key);
    #endif
    return entry;
  }

  return NULL;
}

/*
  Delete entry with key ~key~ in ~hash_table~, this function handles values in
  linked list guaranteeing correct values after deletion of entry.

  On success:
  Return 0.

  On failure:
  It can only fail when no entry with key ~key~ is found. Returns 1.
*/
int hash_delete(struct hash_table table, char key)
{
  int hashed_key = hash_function(key);
  struct hash_entry * current = table.table[hashed_key];

  if (current == NULL)
    return 1;

  free(current);
  return 0;
}

void free_hash(struct hash_table hash)
{
  for (int i = 0; i < hash.size; i++)
    if (hash.table[i] != NULL)
      free(hash.table[i]);
  free(hash.table);
}
