/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "data.h"
#include "structures.h"

extern struct position ** positions;
extern int positions_count;
extern int dataset_position;
extern char ** dataset;

int process_dataset(void)
{
  for (int position_index = 0; position_index < dataset_position; position_index++) {
    char * data = dataset[position_index];
    char character;
    int token_position = 0;

    // start processing token
    while ((character = data[token_position]) != '\0') {
      struct hash_table hash = positions[token_position]->hash;
      struct flexible_array ** elements = &positions[token_position]->elements;
      struct hash_entry * hash_entry = hash_search(hash, character);

      if (hash_entry == NULL) {
        hash_insert(hash, character, 1);
        add_element_to_array(elements, character);
      }
      // the else of the previous if should increment elements to count
      // character for position

      token_position++;
    }

    if (token_position != positions_count)
      fprintf(stderr, "Token: %s differs in size with data, size: %d\n",
              data, token_position);
  }

  return 0;
}
