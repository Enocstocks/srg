/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRUCTURES

#include <stdlib.h>
#include "hash.h"

#define STRUCTURES

#define array_malloc(array, s) \
  array = malloc(sizeof(int) * 2 + (s * sizeof(unsigned int)));  \
  array->size = s;                                              \
  array->position = 0;

struct flexible_array {
  int size;
  int position;
  unsigned int array[];
};

struct position {
  struct hash_table hash; // Counts how many times a charactes has appeared,
                          // it is also used to know if character already exist
                          // in elemets
  struct flexible_array * elements; // Unsorted characters found in this position of the token
};

void realloc_array(struct flexible_array **);

int add_element_to_array(struct flexible_array **, int);

#endif
