/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#include "structures.h"
#include "args.h"
#include "data.h"
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>

#define data_pool_size 64
#define token_size 256

// use only in this file
extern char * dataset_file;
int dataset_realloc_step = 2;
//

struct position ** positions;
int cpu_count, positions_count;
char ** dataset;
unsigned int dataset_position = 0; // number of elements in dataset and also
                                   // last free position in dataset

static int setup_dataset_from_file(void);
static void free_all(void);
static void print_results(void);

int main(int argc, char ** argv)
{
#ifdef __linux__
  cpu_count = get_nprocs();
#else
  fprintf(stderr, "Error: platform not supported, please contact developer\n");
  return 1;
#endif

  dataset = malloc(data_pool_size * sizeof(void *));
  get_flags(argc, argv);

  if (dataset_file)
    setup_dataset_from_file();
  else {
    fprintf(stderr, "Currently an error is raised if no dataset is given\n");
    return 1;
  }

  // Initiate program with N positions reading the first token
  positions_count = strlen(dataset[0]);

  // initialize positions
  positions = malloc(positions_count * sizeof(struct position *));

  for (int i = 0; i < positions_count; i++) {
    struct position ** pos = &positions[i];
    *pos = malloc(sizeof(struct position));
    alloc_hash_table((*pos)->hash, 36); // 36 = 26 english letters + 10 numbers
    array_malloc((*pos)->elements, 16); // 16 to start with a nice size
  }

  process_dataset();
  print_results();
  free_all();
}

static int setup_dataset_from_file(void)
{
  FILE * file = fopen(dataset_file, "r");
  char data[256];

  while (fscanf(file, "%s\n", &data) != EOF) {
    dataset[dataset_position] = malloc(token_size);
    strcpy(dataset[dataset_position], data);
    dataset_position++;

    // increase amount of space for data set values
    if (dataset_position >= 64){
      dataset = realloc(dataset,
                        data_pool_size * sizeof(void *) * dataset_realloc_step);
      dataset_realloc_step++;
    }
  }

  fclose(file);
  return 0;
}

static void free_all(void)
{
  for (unsigned int i = 0; i < dataset_position; i++) {
    free(dataset[i]);
  }
  free(dataset);

  for (int i = 0; i < positions_count; i++) {
    free(positions[i]->elements);
    free_hash(positions[i]->hash);
    free(positions[i]);
  }

  free(positions);
}

void print_results(void)
{
  for (int i = 0; i < positions_count; i++) {
    struct position * pos = positions[i];

    printf("Position %d: ", i + 1);
    for (int x = 0; x < pos->elements->position; x++) {
      printf("%c", pos->elements->array[x]);
    }

    printf(" with size: %d\n", pos->elements->position);
  }
}
