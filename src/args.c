/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <stddef.h>

int debug_flag = 0;
char * dataset_file;

/*
  Check cli arguments and set flags.

  On success:
  Return the index position of argv where the non-option cli arguments begin.

  On failure:
  It fails when a cli option that is not present within options[] is given.
*/

int get_flags(int argc, char ** argv)
{
  int character;

  struct option options[] = {
    {"debug", no_argument, &debug_flag, 1},
    {"dataset", required_argument, NULL, 2},
    {0, 0, 0, 0},
  };

  while ((character = getopt_long(argc, argv, "d", options, (void *) 0)) != -1) {
    if (character == 'd')
      debug_flag = 1;

    if (character == 2)
      dataset_file = optarg;
  }

  return optind;
}
