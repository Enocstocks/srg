/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIMPLE_HASH
#define SIMPLE_HASH 1

#include <stdlib.h>

// Main structure to hold the hash table
struct hash_table {
  struct hash_entry ** table;
  int size;
};

struct hash_entry {
  char key;
  int value;
};

#define alloc_hash_table(hash_table, s) \
  hash_table.table = calloc(s, sizeof(void *));      \
  hash_table.size = s;

int hash_function(char);

int hash_insert(struct hash_table, char, int);

struct hash_entry * hash_search(struct hash_table, char);

int hash_delete(struct hash_table, char);

void free_hash(struct hash_table);

#endif
