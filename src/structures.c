/*
  This file is part of srg.

  Innuendo is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option) any
  later version.

  Innuendo is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
  details.

  You should have received a copy of the GNU Affero General Public License along
  with Innuendo. If not, see <https://www.gnu.org/licenses/>.
*/

#include "structures.h"

void realloc_array(struct flexible_array ** array)
{
  int new_size = (*array)->size * 2;
  struct flexible_array * reallocation;
  (*array)->size = new_size;
  reallocation = realloc(*array, (sizeof(int) * 2) + new_size * sizeof(unsigned int));
  *array = reallocation;
}

int add_element_to_array(struct flexible_array ** array, int element)
{
  if ((*array)->position >= (*array)->size)
    realloc_array(array);

  (*array)->array[(*array)->position] = element;
  (*array)->position += 1;
  return 0;
}
